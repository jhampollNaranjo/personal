function limpiar_tabla() {
	var contenedor = document.getElementById('contenedor');
	while (contenedor.firstChild) {
  contenedor.removeChild(contenedor.firstChild);
	}
}

//https://www.youtube.com/watch?v=Jd4G8zz0GHQv


function leapYear() {
	var year = document.getElementById("years").value;
	if ((year % 4 == 0) && !(year % 400 == 0)) {
		bisiesto = true;
	} else if ((year % 100 == 0) && (year % 400 == 0)) {
		bisiesto = true;
	} else {
		bisiesto = false;
	}
	return bisiesto;
}

function daysOfthemonts(month) {

	var days;
	switch (parseInt(month)) {
		case 1:days = 31;break;
		case 2:
			if (leapYear()) {
				days = 29;
			} else {
				days = 28;
			}
      break;
		case 3:days = 31;break;
		case 4:days = 30;break;
		case 5:days = 31;break;
		case 6:days = 30;break;
		case 7:days = 31;break;
		case 8:days = 31;break;
		case 9:days = 30;break;
		case 10:days = 31;break;
		case 11:days = 30;break;
		case 12:days = 31;break;
    default:
	}
	return days;
}

function genera_tabla(month) {

	var contenedor = document.getElementById('contenedor');
	var Dias = ["D", "L", "M", "M", "J", "V", "S"];
	var month_text = ["january", "February", "March", "April", "May", "June", "July", "August", "September", "Octuber", "November", "Dicember"];

	var div = document.createElement("DIV");
	div.setAttribute("class", "col-md-4 table-responsive");
	var p = document.createElement("h2");
	var textomes = document.createTextNode(month_text[month - 1]);
	p.appendChild(textomes);
	var tabla = document.createElement("table");
	var tblhead = document.createElement("thead");

	var hileraDias = document.createElement("tr");
	for (var i = 0; i < Dias.length; i++) {

		var celdaDias = document.createElement("th");
		var textoDialunes = document.createTextNode(Dias[i]);
		celdaDias.appendChild(textoDialunes);
		hileraDias.appendChild(celdaDias);
	}

	tblhead.appendChild(hileraDias);
	tabla.appendChild(tblhead);
	var tblBody = document.createElement("tbody");
	tabla.setAttribute("class", "table  table-dark");
	tabla.setAttribute("height","350px");
	var contador = 1;
	var year = document.getElementById("years").value;
	var bandera = true;
	var m = parseInt(cala_weekday(month, 1, year));

	for (var i = 0; i < 6; i++) {
		var hilera = document.createElement("tr");

		for (var j = 0; j < 7; j++) {
			var celda = document.createElement("td");
			var l = daysOfthemonts(month);

			if (contador <= l) {
				if (j == m || bandera == false) {
					var textoCelda = document.createTextNode(contador);
					celda.appendChild(textoCelda);
					bandera = false;
					contador = contador + 1;
				}
			}
			hilera.appendChild(celda);
		}
		tblBody.appendChild(hilera);
	}

	tabla.appendChild(tblBody);
	div.appendChild(p);
	div.appendChild(tabla);
	contenedor.appendChild(div);


}

function calendar() {
	limpiar_tabla();
	for (var i = 1; i <= 12; i++) {
		genera_tabla(i);
	}

}

function cala_weekday(x_nMonth, x_nDay, x_nYear) {

	if (x_nMonth >= 3) {
		x_nMonth -= 2;
	} else {
		x_nMonth += 10;
	}

	if ((x_nMonth == 11) || (x_nMonth == 12)) {
		x_nYear--;
	}

	var nCentNum = parseInt(x_nYear / 100);
	var nDYearNum = x_nYear % 100;

	var g = parseInt(2.6 * x_nMonth - .2);

	g += parseInt(x_nDay + nDYearNum);
	g += nDYearNum / 4;
	g = parseInt(g);
	g += parseInt(nCentNum / 4);
	g -= parseInt(2 * nCentNum);
	g %= 7;

	if (x_nYear >= 1700 && x_nYear <= 1751) {
		g -= 3;
	} else {
		if (x_nYear <= 1699) {
			g -= 4;
		}
	}

	if (g < 0) {
		g += 7;
	}

	return g;
}
